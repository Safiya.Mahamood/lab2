package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

    public class Fridge implements IFridge{
    private final int maxCapacity = 20;
    private ArrayList<FridgeItem>items;

    public Fridge(){
        items = new ArrayList<FridgeItem>();
    }

    @Override
    public int nItemsInFridge(){
        return items.size();
    }

    @Override
    public int totalSize(){
        return maxCapacity;
    }

    @Override
    public boolean placeIn(FridgeItem item){
        if(totalSize() - nItemsInFridge() > 0){//ifitsmorethan0meaningnitemscurrenetlyinfridgehaselementsthenadditem
            items.add(item);
        }
        else{
        return false;
        }
        return true;
    }

    @Override
    public void takeOut(FridgeItem item){
        if(items.contains(item)){//ifthelistcontainsitemremovethembasedonwhatitemwewanttoremove
            items.remove(item);
        }
        else{
        throw new NoSuchElementException("Fridgedoesnotcontain"+ item);
        }
    }

    @Override
    public void emptyFridge(){
        items.clear();
    }

    @Override
    public List<FridgeItem>removeExpiredFood(){
        List<FridgeItem>expired = new ArrayList<>();//usingobjectfridgeitemwewantthemethodhasexpiredtothencompareifitemsinourexistinglisthasanyexpireditemsandifsoremovethem
        for (FridgeItem item : items){
            if(item.hasExpired()){
                expired.add(item);
            }
        }
        items.removeAll(expired);
        return expired;
    }
}
